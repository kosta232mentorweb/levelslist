export default [
    {
        id: 'LVL0114',
        title: {
            key: 'find_cat',
            text: 'Найди котика'
        },
        created: {
            author: 'admin',
            date: '18.03.2020'
        },
        edited: {
            author: 'admin',
            date: '18.03.2020'
        },
        tested: {
            author: 'admin',
            date: '18.03.2020'
        },
        status: 'готов для теста',
        optional: {
            isOptional: false,
            parentId: ''
        },
        availability: {
            isAvailability: true,
            text: ''
        }
    },
    {
        id: 'LVL0197',
        title: {
            key: 'find_dog',
            text: 'Найди собачку'
        },
        created: {
            author: 'admin',
            date: '18.03.2020'
        },
        edited: {
            author: 'admin',
            date: '18.03.2020'
        },
        tested: {
            author: 'admin',
            date: '18.03.2020'
        },
        status: 'протестирован',
        optional: {
            isOptional: true,
            parentId: 'LVL0001'
        },
        availability: {
            isAvailability: false,
            text: 'заблокирован админом'
        }
    },
    {
        id: 'LVL1294',
        title: {
            key: 'find_turtle',
            text: 'Найди черепаху'
        },
        created: {
            author: 'admin',
            date: '18.03.2020'
        },
        edited: {
            author: 'admin',
            date: '18.03.2020'
        },
        tested: {
            author: 'admin',
            date: '18.03.2020'
        },
        status: 'протестирован',
        optional: {
            isOptional: true,
            parentId: 'LVL0104'
        },
        availability: {
            isAvailability: false,
            text: 'заблокирован админом'
        }
    },
    {
        id: 'LVL0001',
        title: {
            key: 'find_bird',
            text: 'Найди птичку'
        },
        created: {
            author: 'admin',
            date: '18.03.2020'
        },
        edited: {
            author: 'admin',
            date: '18.03.2020'
        },
        tested: {
            author: 'admin',
            date: '18.03.2020'
        },
        status: 'ошибки',
        optional: {
            isOptional: false,
            parentId: ''
        },
        availability: {
            isAvailability: true,
            text: ''
        }
    },
    {
        id: 'LVL0104',
        title: {
            key: 'find_pet',
            text: 'Найди хомячка'
        },
        created: {
            author: 'admin',
            date: '18.03.2020'
        },
        edited: {
            author: 'admin',
            date: '18.03.2020'
        },
        tested: {
            author: 'admin',
            date: '18.03.2020'
        },
        status: 'готов для теста',
        optional: {
            isOptional: false,
            parentId: ''
        },
        availability: {
            isAvailability: true,
            text: ''
        }
    },
    {
        id: 'LVL0204',
        title: {
            key: 'find_pet',
            text: 'Найди хомячка'
        },
        created: {
            author: 'admin',
            date: '18.03.2020'
        },
        edited: {
            author: 'admin',
            date: '18.03.2020'
        },
        tested: {
            author: 'admin',
            date: '18.03.2020'
        },
        status: 'на редактуре',
        optional: {
            isOptional: false,
            parentId: ''
        },
        availability: {
            isAvailability: false,
            text: 'кем-то редактируется'
        }
    },
    {
        id: 'LVL0218',
        title: {
            key: 'find_pet2',
            text: 'Найди хомячка 2'
        },
        created: {
            author: 'admin',
            date: '18.03.2020'
        },
        edited: {
            author: 'admin',
            date: '18.03.2020'
        },
        tested: {
            author: 'admin',
            date: '18.03.2020'
        },
        status: 'на редактуре',
        optional: {
            isOptional: false,
            parentId: ''
        },
        availability: {
            isAvailability: true,
            text: 'кем-то редактируется'
        }
    },
    {
        id: 'LVL0219',
        title: {
            key: 'find_pet2',
            text: 'Найди хомячка 2'
        },
        created: {
            author: 'admin',
            date: '18.03.2020'
        },
        edited: {
            author: 'admin',
            date: '18.03.2020'
        },
        tested: {
            author: 'admin',
            date: '18.03.2020'
        },
        status: 'на редактуре',
        optional: {
            isOptional: false,
            parentId: ''
        },
        availability: {
            isAvailability: true,
            text: 'кем-то редактируется'
        }
    }
]
